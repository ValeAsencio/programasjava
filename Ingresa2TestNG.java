
package Ingresa2;

import java.util.Scanner;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * captura el numero del teclado
 */

public class Ingresa2TestNG{
  /**
  * 
  * lee el teclado
  */	
  public static  Scanner teclado;
  /**
   * metodo principal
   */
  public static void main(final String args[ ]) {
    teclado = new Scanner(System.in);
    final long binario = leeBinario();
    final int decimal = convierteDecimal(binario);
    imprime(binario,decimal);
  }
  /**
   * lee el numero para ver si es binario
   */
  @BeforeMethod
  public static long leeBinario(){
  	final Exception error1 = new Exception("Lo que se ingreso no es un binario");
    String num = "";
    boolean sigue = true; 
    while(sigue){
      try{
         System.out.print("Ingrese numero binario: ");
         num = teclado.nextLine().trim();
         if(noEsBinario(num)){
           throw error1;
         }
         else{
           sigue = false;
         }
    }
    
    
    catch(Exception ex){
        System.out.println(ex.getMessage()); // NOPMD by Laboratorio on 27/04/17 15:34
    }
    
    }
    return Long.parseLong(num);
  }
  /**
   * Verifica cuando el numero que ingresa no es binario
   */
  @Test(timeOut=500)
  public static boolean noEsBinario(final String num){
    boolean noEs = false;
    final int largo = num.length();
    int pal = 0;
    while(pal < largo && !noEs){
      if( !( num.substring(pal,pal+1).equals("0") || num.substring(pal,pal+1).equals("1") ) ){
        noEs = true;
      }
      pal++;
    }
    return noEs;
  }
  /**
   * convierte el numero a decimal
   */
  @AfterMethod
  public static int convierteDecimal(long bina){
	//se asigna a una variable temporal el long bi
	long bina2=bina;  
    final String bnr = String.valueOf(bina2);
    //era int largo, y se pone final porque la variable no cambia dentro del metodo   
    final int largo = bnr.length();
    int decimal = 0;
    int des;
    for(int i = largo-1; i >= 0; i--){
      des = (int)(bina2/Math.pow(10,i));
      decimal = decimal + des*(int)Math.pow(2,i);
      bina2 = (long)(bina2%Math.pow(10,i));
    }
    return decimal;
  }
  /**
  * imprime el resultado
  */
  
  public static void imprime(final long bina, final int decimal){
    System.out.println("El equivalente decimal del binario "+bina+" es: "+decimal);
  }
}