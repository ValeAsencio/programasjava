package Class;
public class ingresa2{
	
	public static void  leeBinario(String n){
	boolean sigue = true;
  	Exception error1 = new Exception("Lo que se ingreso no es un binario");
    while(sigue){
      try{
         //System.out.print("Ingrese numero binario: ");
         //n = teclado.nextLine().trim();
         if(noEsBinario(n)){
           throw error1;
           
         }
         else{
           sigue = false;
           long binario =  Long.parseLong(n);
           int decimal = convierteDecimal(binario);
           imprime(binario,decimal);
           
         }
    }
    
      catch(Exception ex){
        System.out.println(ex.getMessage());
        sigue = false;
        
    }
    
    }
    
  }
  
  public static boolean noEsBinario(String n){
    boolean NoEs = false;
    int largo = n.length();
    int p = 0;
    while(p < largo && !NoEs){
      if(!((n.substring(p,p+1).equals("0") || (n.substring(p,p+1).equals("1"))))){
        NoEs = true;
      }
      p++;
    }
    return NoEs;
  }
  
  public static int convierteDecimal(long bi){
    String b = String.valueOf(bi);
    int largo = b.length();
    int decimal = 0;
    int des = 0;
    for(int i = largo-1; i >= 0; i--){
      des = (int)(bi/Math.pow(10,i));
      decimal = decimal + des*(int)Math.pow(2,i);
      bi = (long)(bi%Math.pow(10,i));
    }
    return decimal;
  }
  
  public static void imprime(long bi, int decimal){
    System.out.println("El equivalente decimal del binario "+bi+" es: "+decimal);
  }
}
